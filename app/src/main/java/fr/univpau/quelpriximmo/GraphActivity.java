package fr.univpau.quelpriximmo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class GraphActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Répartition des prix");

        BarChart chart = findViewById(R.id.barchart);

        ArrayList values = new ArrayList();

        Intent it =  getIntent();

        values.add(new BarEntry(it.getIntExtra("0a50", 0), 0));
        values.add(new BarEntry(it.getIntExtra("50a100", 0), 1));
        values.add(new BarEntry(it.getIntExtra("100a150", 0), 2));
        values.add(new BarEntry(it.getIntExtra("150a200", 0), 3));
        values.add(new BarEntry(it.getIntExtra("200a300", 0), 4));
        values.add(new BarEntry(it.getIntExtra("300a400", 0), 5));
        values.add(new BarEntry(it.getIntExtra("40a500", 0), 6));
        values.add(new BarEntry(it.getIntExtra("plus500", 0), 7));

        ArrayList labels = new ArrayList();

        labels.add("0 à 50K €");
        labels.add("50K à 100K €");
        labels.add("100K à 150K €");
        labels.add("150K à 200K €");
        labels.add("200K à 300K €");
        labels.add("300K à 400K €");
        labels.add("400K à 500K €");
        labels.add("+ de 500K €");
        BarDataSet bardataset = new BarDataSet(values, "Nombre de biens");

        bardataset.setValueTextSize(10f);

        chart.animateY(1000);
        chart.getLegend().setTextSize(10f);
        chart.getXAxis().setTextSize(8f);
        chart.setDrawGridBackground(false);

        chart.setDescription("Répartition des biens par prix");

        BarData data = new BarData(labels, bardataset);

        bardataset.setColors(new int[]{R.color.chart1 , R.color.chart2, R.color.chart3, R.color.chart4, R.color.chart5, R.color.chart6, R.color.chart7, R.color.chart8}, this);
        chart.setData(data);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            //Title bar back press triggers onBackPressed()
            onBackPressed();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0 ) {
            getFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
        }
    }
}