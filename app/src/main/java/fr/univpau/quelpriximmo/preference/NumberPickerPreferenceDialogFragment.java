package fr.univpau.quelpriximmo.preference;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NumberPickerPreferenceDialogFragment extends CustomPreferenceDialogFragmentCompat {
    private static final String ARG_MIN_VALUE = "min_value";
    private static final String ARG_MAX_VALUE = "max_value";
    private static final String ARG_UNIT_TEXT = "unit_text";
    private static final String SAVE_STATE_TIME = "save_state_time";
    private static final String ARG_STEP_VALUE = "step_value";
    private String[] displayedValues;

    protected NumberPicker np;

    public static NumberPickerPreferenceDialogFragment newInstance(
            @NonNull final String key,
            final Integer minValue,
            final Integer maxValue,
            @Nullable final String unitText,
            Integer stepValue
    ) {
        final NumberPickerPreferenceDialogFragment fragment = new NumberPickerPreferenceDialogFragment();
        final Bundle b = new Bundle(4);
        b.putString(ARG_KEY, key);
        b.putInt(ARG_MIN_VALUE, minValue);
        b.putInt(ARG_MAX_VALUE, maxValue);
        b.putString(ARG_UNIT_TEXT, unitText);
        b.putInt(ARG_STEP_VALUE, stepValue);

        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Integer text;
        if (savedInstanceState == null) {
            text = getNumberPickerDialogPreference().getSerializedValue();
        } else {
            text = savedInstanceState.getInt(SAVE_STATE_TIME);
        }

        // Init Picker
        final int minValue = getArguments().getInt(ARG_MIN_VALUE, 0);
        final int maxValue = getArguments().getInt(ARG_MAX_VALUE, 100);
        final int stepValue = getArguments().getInt(ARG_STEP_VALUE, 0);

        displayedValues  = new String[maxValue];

        for(int i=minValue; i<maxValue; i++)
            displayedValues[i] = String.valueOf(stepValue * (i+1)) + " m";

        np = new NumberPicker(getActivity());

        np.setMinValue(minValue);
        np.setMaxValue(displayedValues.length - 1);
        np.setDisplayedValues(displayedValues);

        for( int i=minValue; i<displayedValues.length ; i++ ) {
            if (displayedValues[i].equals(text + " m")) {
                np.setValue(i);
            }
        }

    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            Integer value = np.getValue();
            Integer choosenValue = Integer.parseInt(displayedValues[value].replace(" m",""));

            if (getNumberPickerDialogPreference().callChangeListener(choosenValue)) {
                getNumberPickerDialogPreference().setSerializedValue(choosenValue);
            }
        }
    }

    @Override
    View getPickerView() {
        return np;
    }

    private NumberDialogPreference getNumberPickerDialogPreference() {
        return (NumberDialogPreference) getPreference();
    }
}