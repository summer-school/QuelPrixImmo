package fr.univpau.quelpriximmo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;
import androidx.preference.PreferenceManager;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.slider.LabelFormatter;
import com.google.android.material.slider.Slider;
import com.google.android.material.snackbar.Snackbar;


public class SearchFragment extends Fragment {

    public MaterialButton AppartButton ;
    public MaterialButton HouseButton ;
    public MaterialButton UnDeuxButton ;
    public MaterialButton TroisQuatreButton;
    public MaterialButton CinqSixButton;
    public MaterialButton SeptHuitButton;
    public MaterialButton PlusHuitButton;
    public MaterialButton searchButton;

    public Slider sliderRange;

    boolean isFirstLaunch = true;




    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.setGroupVisible(R.id.mapMenu, false);
        menu.setGroupVisible(R.id.sortMenu, false);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)this.getActivity()).setCurrentFragment(this);

        getActivity().setTitle("Recherche de biens");

        sliderRange = view.findViewById(R.id.sliderRange);
        AppartButton = view.findViewById(R.id.AppartButton);
        HouseButton = view.findViewById(R.id.HouseButton);

        UnDeuxButton = view.findViewById(R.id.UnDeuxButton);
        TroisQuatreButton = view.findViewById(R.id.TroisQuatreButton);
        CinqSixButton = view.findViewById(R.id.CinqSixButton);
        SeptHuitButton = view.findViewById(R.id.SeptHuitButton);
        PlusHuitButton = view.findViewById(R.id.PlusHuitButton);
        searchButton = view.findViewById(R.id.searchButton);

        sliderRange.setLabelFormatter(new SliderFormatter());

        // Si l'utilisateur à définis un rayon par défaut dans les pref, on le charge
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        Integer savedRayon = settings.getInt("rayonPreference", 500);
        sliderRange.setValue(savedRayon);

        if(isFirstLaunch) {
            isFirstLaunch = false;
            Snackbar.make(view, "Rayon de recherche défini par défaut à " + savedRayon + "m", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }

        // A chaque changement d'état de boutons, on execute la validation de formulaire
        AppartButton.setOnClickListener(new UpdateListener(this));
        HouseButton.setOnClickListener(new UpdateListener(this));
        UnDeuxButton.setOnClickListener(new UpdateListener(this));
        TroisQuatreButton.setOnClickListener(new UpdateListener(this));
        CinqSixButton.setOnClickListener(new UpdateListener(this));
        SeptHuitButton.setOnClickListener(new UpdateListener(this));
        PlusHuitButton.setOnClickListener(new UpdateListener(this));


        searchButton.setOnClickListener(new SearchListener());

        validateForm(view, this);

    }

    @Override
    public void onResume()
    {
        super.onResume();
        validateForm(this.getView(), this);

        // Si l'utilisateur à définis un rayon par défaut dans les pref, on le charge
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        Integer savedRayon = settings.getInt("rayonPreference", 500);
        sliderRange.setValue(savedRayon);
    }


    private void validateForm(View v, SearchFragment fragment)
    {
        if(!fragment.AppartButton.isChecked() && !fragment.HouseButton.isChecked())
        {
            fragment.searchButton.setEnabled(false);
            return;
        }

        if(!fragment.UnDeuxButton.isChecked() && !fragment.TroisQuatreButton.isChecked() && !fragment.CinqSixButton.isChecked() && !fragment.SeptHuitButton.isChecked() && !fragment.PlusHuitButton.isChecked())
        {
            fragment.searchButton.setEnabled(false);
            return;
        }

        fragment.searchButton.setEnabled(true);
    }


    private class UpdateListener implements View.OnClickListener
    {
        private SearchFragment fragment;
        public UpdateListener(SearchFragment fragment) {
            this.fragment = fragment;
        }

        @Override
        public void onClick(View v) {
            validateForm(v, fragment);
        }
    }

    private class SearchListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            bundle.putInt("rayon", Math.round(sliderRange.getValue()));
            bundle.putBoolean("appartement", AppartButton.isChecked());
            bundle.putBoolean("maison", HouseButton.isChecked());

            bundle.putBoolean("unDeux", UnDeuxButton.isChecked());
            bundle.putBoolean("troisQuatre", TroisQuatreButton.isChecked());
            bundle.putBoolean("cinqSix", CinqSixButton.isChecked());
            bundle.putBoolean("septHuit", SeptHuitButton.isChecked());
            bundle.putBoolean("plusHuit", PlusHuitButton.isChecked());


            NavHostFragment.findNavController(SearchFragment.this)
                    .navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);
        }
    }

    private class SliderFormatter implements LabelFormatter
    {
        @NonNull
        @Override
        public String getFormattedValue(float value) {
            return Math.round(value) + " m";
        }
    }

}

