package fr.univpau.quelpriximmo;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import fr.univpau.quelpriximmo.adapter.ImmoAdapter;
import fr.univpau.quelpriximmo.model.BienImmo;
import fr.univpau.quelpriximmo.tool.Toolkit;

public class ResultListFragment extends Fragment implements LocationListener{
    ListView mListView;
    List<BienImmo> biensImmo;
    ImmoAdapter adapter;

    Double lat;
    Double lng;

    Boolean shouldIncludeAppartement;
    Boolean shouldIncludeMaison;

    Boolean shouldIncludeUnDeux;
    Boolean shouldIncludeTroisQuatre;
    Boolean shouldIncludeCinqSix;
    Boolean shouldIncludeSeptHuit;
    Boolean shouldIncludeHuitPlus;

    Integer rayonToSearch;


    FusedLocationProviderClient fusedLocationClient;
    Thread thread;

    Boolean first = true;


    Boolean distanceDESC = false;
    Boolean priceDESC  = false;
    Boolean sqmetersDESC  = false;
    Boolean roomDESC  = false;
    Boolean dateDESC  = false;


    @Override
    public void onPause()
    {
        super.onPause();
        ((MainActivity)this.getActivity()).toggleGraphActionButton(View.INVISIBLE);

    }

    public void doSort(String sortType)
    {
        if(sortType.equals("sortDistance"))
        {
            if(distanceDESC) {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        return o1.getDistanceToMe().compareTo(o2.getDistanceToMe());
                    }
                });
                distanceDESC = false;
            }
            else
            {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        return o2.getDistanceToMe().compareTo(o1.getDistanceToMe());
                    }
                });
                distanceDESC = true;
            }

            adapter = new ImmoAdapter(getContext(), biensImmo);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        if(sortType.equals("sortDate"))
        {
            if(dateDESC) {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        return o1.getDate().compareTo(o2.getDate());
                    }
                });
                dateDESC = false;
            }
            else
            {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        return o2.getDate().compareTo(o1.getDate());
                    }
                });
                dateDESC = true;
            }

            adapter = new ImmoAdapter(getContext(), biensImmo);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        if(sortType.equals("sortPrice"))
        {
            if(priceDESC) {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        return o1.getPrix().compareTo(o2.getPrix());
                    }
                });
                priceDESC = false;
            }
            else
            {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        return o2.getPrix().compareTo(o1.getPrix());
                    }
                });
                priceDESC = true;
            }

            adapter = new ImmoAdapter(getContext(), biensImmo);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        if(sortType.equals("sortRooms"))
        {
            if(roomDESC) {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        return o1.getnPiece().compareTo(o2.getnPiece());
                    }
                });
                roomDESC = false;
            }
            else
            {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        return o2.getnPiece().compareTo(o1.getnPiece());
                    }
                });
                roomDESC = true;
            }

            adapter = new ImmoAdapter(getContext(), biensImmo);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        if(sortType.equals("sortSquareMeterPrice"))
        {
            if(sqmetersDESC) {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        Integer b1 = (o1.getPrix() / o1.getSurface());
                        Integer b2 = (o2.getPrix() / o2.getSurface());

                        return b1.compareTo(b2);
                    }
                });
                sqmetersDESC = false;
            }
            else
            {
                Collections.sort(biensImmo, new Comparator<BienImmo>() {
                    @Override
                    public int compare(BienImmo o1, BienImmo o2) {
                        Integer b1 = (o1.getPrix() / o1.getSurface());
                        Integer b2 = (o2.getPrix() / o2.getSurface());

                        return b2.compareTo(b1);
                    }
                });
                sqmetersDESC = true;
            }

            adapter = new ImmoAdapter(getContext(), biensImmo);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_resultlist, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)this.getActivity()).setCurrentFragment(this);
        ((MainActivity)this.getActivity()).toggleGraphActionButton(View.VISIBLE);

        shouldIncludeAppartement = getArguments().getBoolean("appartement");
        shouldIncludeMaison = getArguments().getBoolean("maison");
        shouldIncludeUnDeux = getArguments().getBoolean("unDeux");
        shouldIncludeTroisQuatre = getArguments().getBoolean("troisQuatre");
        shouldIncludeCinqSix = getArguments().getBoolean("cinqSix");
        shouldIncludeSeptHuit = getArguments().getBoolean("septHuit");
        shouldIncludeHuitPlus = getArguments().getBoolean("plusHuit");


        rayonToSearch = getArguments().getInt("rayon");


        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getActivity().setTitle("Liste des biens");

        ProgressDialog progress = new ProgressDialog(view.getContext());
        progress.setTitle("Chargement");
        progress.setMessage("Chargement des biens à proximité ...");
        progress.setCancelable(false);
        progress.show();

        mListView = view.findViewById(R.id.listViewImmo);

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    biensImmo = getBienImmos(view, lat, lng, rayonToSearch);
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            adapter = new ImmoAdapter(view.getContext(), biensImmo);
                            mListView.setAdapter(adapter);

                            Integer size = biensImmo.size();

                            TextView numberResult = view.findViewById(R.id.numberResult);
                            numberResult.setText(NumberFormat.getNumberInstance(Locale.FRANCE).format(size) + " biens trouvés dans un rayon de " + NumberFormat.getNumberInstance(Locale.FRANCE).format(rayonToSearch) + "m");

                            ((MainActivity)getActivity()).setCurrentData(biensImmo, lat, lng);

                            progress.dismiss();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        // view.findViewById(R.id.searchButton).setOnClickListener(new View.OnClickListener() {
        //   @Override
        //   public void onClick(View view) {
        //        NavHostFragment.findNavController(ResultListFragment.this)
        //               .navigate(R.id.action_SecondFragment_to_FirstFragment);
        //  }
        //});
    }

    private List<BienImmo> getBienImmos(View view, Double lat, Double lng, Integer dist) throws IOException, JSONException {
        List<BienImmo> biens = new ArrayList<>();

        URL url = new URL("https://api.cquest.org/dvf"
                + "?lat=" + lat.toString()
                + "&lon=" + lng.toString()
                + "&dist=" + dist.toString());
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        // urlConnection.setReadTimeout(15000);
        // urlConnection.setConnectTimeout(15000);

        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String content = readStream(in);

            JSONObject rootObj = new JSONObject(content);
            JSONArray jArray = rootObj.getJSONArray("features");

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject bn = jArray.getJSONObject(i).getJSONObject("properties");
                BienImmo immo = new BienImmo();

                if(bn.has("valeur_fonciere"))
                    immo.setPrix(bn.getInt("valeur_fonciere"));
                else
                    immo.setPrix(-1);

                StringBuilder builderAddress = new StringBuilder();

                if(bn.has("numero_voie"))
                {
                    builderAddress.append(bn.getString("numero_voie"));
                }

                if(bn.has("type_voie"))
                {
                    builderAddress.append(" ");
                    builderAddress.append(bn.getString("type_voie"));
                }

                if(bn.has("voie"))
                {
                    builderAddress.append(" ");
                    builderAddress.append(bn.getString("voie"));
                }

                if(bn.has("code_postal"))
                {
                    builderAddress.append(", ");
                    builderAddress.append(bn.getString("code_postal"));
                }

                if(bn.has("commune"))
                {
                    builderAddress.append(", ");
                    builderAddress.append(bn.getString("commune"));
                }

                immo.setFormattedAddress(toDisplayCase(builderAddress.toString()));

                if(bn.has("lat"))
                    immo.setLat(bn.getDouble("lat"));
                else
                    immo.setLat(-1d);

                if(bn.has("lon"))
                    immo.setLng(bn.getDouble("lon"));
                else
                    immo.setLng(-1d);

                if(bn.has("type_local"))
                    immo.setType(bn.getString("type_local"));

                if(bn.has("nombre_pieces_principales"))
                    immo.setnPiece(bn.getInt("nombre_pieces_principales"));
                else
                    immo.setnPiece(-1);

                if(bn.has("surface_relle_bati"))
                    immo.setSurface(bn.getInt("surface_relle_bati"));
                else
                    immo.setSurface(-1);

                SimpleDateFormat parser= new SimpleDateFormat("yyyy-MM-dd");

                if(bn.has("date_mutation"))
                    immo.setDate(parser.parse(bn.getString("date_mutation"), new ParsePosition(0)));

                Double distToMe = Toolkit.distance(immo.getLat(), immo.getLng(), lat, lng, 'M');

                immo.setDistanceToMe((int)Math.round(distToMe));


                String typeImmo = immo.getType();

                if(typeImmo == null)
                    continue;
                if(typeImmo != null && typeImmo.equals("Appartement") && !shouldIncludeAppartement)
                    continue;
                else if(typeImmo != null && typeImmo.equals("Maison") && !shouldIncludeMaison)
                    continue;
                else if(typeImmo != null && !typeImmo.equals("Maison") && !typeImmo.equals("Appartement"))
                    continue;

                Integer nbPiece = immo.getnPiece();

                if(nbPiece == null)
                    continue;
                if(nbPiece != null && (nbPiece == 1 || nbPiece == 2) && !shouldIncludeUnDeux)
                    continue;
                if(nbPiece != null && (nbPiece == 3 || nbPiece == 4) && !shouldIncludeTroisQuatre)
                    continue;
                if(nbPiece != null && (nbPiece == 5 || nbPiece == 6) && !shouldIncludeCinqSix)
                    continue;
                if(nbPiece != null && (nbPiece == 7 || nbPiece == 8) && !shouldIncludeSeptHuit )
                    continue;
                if(nbPiece != null && (nbPiece > 8) && !shouldIncludeHuitPlus)
                    continue;

                if(!bn.has("nature_mutation") || !bn.getString("nature_mutation").equals("Vente"))
                    continue;

                biens.add(immo);
            }


            Collections.sort(biens, new Comparator<BienImmo>() {
                @Override
                public int compare(BienImmo o1, BienImmo o2) {
                    return o1.getDistanceToMe().compareTo(o2.getDistanceToMe());
                }
            });

            return biens;
        } finally {
            urlConnection.disconnect();
        }
    }

    public static String toDisplayCase(String s) {

        final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
        // to be capitalized

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray()) {
            c = (capNext)
                    ? Character.toUpperCase(c)
                    : Character.toLowerCase(c);
            sb.append(c);
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0); // explicit cast not needed
        }
        return sb.toString();
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private LocationManager mLocationManager;

    @Override
    public void onLocationChanged(@NonNull Location location) {
        lat = round(location.getLatitude(), 2);
        lng = round(location.getLongitude(), 2);

        if(first)
        {
            first = false;
            thread.start();
        }

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onResume() {
        super.onResume();
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        ((MainActivity)this.getActivity()).toggleGraphActionButton(View.VISIBLE);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {


    }
}